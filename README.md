# Brief Overview of Macwire

Macwire is a compile time dependency injection framework. If you are coming from the Spring background, it defers from Spring as Spring is a runtime Dependency Injection framework.
Compile Time DI offers a few advantages, majorly Type Safety.
In runtime DI, an invalid injection would result in a runtime error. With Macwire, illegitimate injections are caught at compile time itself, thus providing a greater degree of safety net. Also compile time DI are relatively known to be light weight providing a slight startup-time benefit over runtime DI which has to scan the classes and inject at runtime.

At the sametime, macwire replaces most of the hand written boilerplate, that would have been the case with manual DI. This is done using Scala Macros. Let’s consider a simple example:

```scala
class UserDao(jdbcSession: JdbcSession)
class UserService(userDao: UserDao)
class UserController(userService: UserService, userDao: UserDao)

object Prac {
 def main(args: Array[String]): Unit = {
   import com.softwaremill.macwire._
   val session:JdbcSession = null

   lazy val userDao = wire[UserDao]
   lazy val userService = wire[UserService]
   lazy val userController = wire[UserController]
 }
}
```

We declare our service and controllers in a usual fashion, expecting constructor arguments. 

When we do `wire[UserDao]`:
at compile time it searches for an instance of `UserDao`. 
If it is not yet created, it attempts to create it:
It observes that UserDao expects a JdbcSession argument. So it looks up for an instance of `JdbcSession` in the scope. We already have an object `session: JdbcSession` in the scope
Using `session` as the constructor, it initializes UserDao

So it is equivalent to:

```scala
   lazy val userDao = new UserDao(session)
   lazy val userService = new UserService(userDao)
   lazy val userController = new UserController(userService, userDao)
```

The bytecode at runtime in both the cases would be similar. A quick way to verify this behavior is by converting above User* classes to case classes and printing `userController`. 

A few points to take note:
* In case any dependency is missing or not available in the current scope, this would result in compile time error. You can try this by commenting `userDao`
* Usage of the `wire` macro can be mixed with creating new instances by hand. Like we did this with the `session` object above
* `wire` works in the below fashion to determine the dependencies needed:
    * In a class, `wire` macro first tries to find a constructor annotated with `@Inject` (javax.inject.Inject)
    * Then the non-primary primary constructor
    * Finally an `apply` method in the companion object
* In case there are multiple dependencies of the same type in scope, then it results in compile time failure. 
For example, if there were multiple instances of `userDao` in the scope:

```scala
lazy val userDao2: UserDao = null
```

then initialization of `userService` and `userController` would fail
* In our example, all the objects defined would be singleton in the scope of usage. Of course they aren’t singleton in the global sense as we can create multiple copies. 
* In case if we we wish to create new instances of a dependency for ease usage (`dependent scope`), we can declare our variables as `def` instead of `lazy val`

There are many more feature available like: dealing with implicits parameters, Interceptors, wiring using factory pattern etc. To know more about macwire, please refer: http://di-in-scala.github.io/. It's a quick read.
